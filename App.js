import React, { Component } from 'react';
import { View, Dimensions, Button } from 'react-native';
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';
import axios from "axios";
const { height, width} = Dimensions.get('window')
export default class App extends Component {
 constructor(props) {
   super(props);
   this.state = {
     apiKey : '46270752',
     sessionId : '',
     token : '',
     recording : false
   }
 }

async componentDidMount(){
   let response = await axios.get('http://192.168.100.136:3000/');
   await this.setState({sessionId : response.data.sessionId, token : response.data.tokenId})
 }

 opentok = () => {
   if(this.state.sessionId != '' && this.state.tokenId != '')
   {
     return(<OTSession apiKey={this.state.apiKey} sessionId={this.state.sessionId} token={this.state.token}>
     <OTPublisher style={{ width: width, height: (height/3) }} />
     <OTSubscriber style={{ width: width, height: (height/3) }} />
   </OTSession>)
   }
 }

 startRecording = async() => {
   this.setState({recording : true})
   let response = await axios.get(`http://192.168.100.136:3000/start`)
   console.log(response);
 }
 stopRecording = async() => {
   this.setState({recording : false})
   let response = await axios.get(`http://192.168.100.136:3000/stop`)
   console.log(response);
 }
 getRecording = async() => {
   let response = await axios.get(`http://192.168.100.136:3000/getarchive`)
   console.log(response);
 }

 render() {
   return (
     <View style={{ flex: 1 }}>
       {this.opentok()}
       <Button title = {this.state.recording ? 'stop' : 'start'} onPress = {this.state.recording ? this.stopRecording :this.startRecording} />
       <Button title = 'get recording' onPress = {this.getRecording} />
     </View>
   );
 }
}